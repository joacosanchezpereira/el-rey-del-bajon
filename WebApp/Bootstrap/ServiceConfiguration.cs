﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Bootstrap
{
    public static class ServiceConfiguration
    {
        public static IServiceCollection ConfigureServices(
           this IServiceCollection services,
           IConfiguration configuration)
        {
            return services;
        }

        public static IApplicationBuilder Configure(this IApplicationBuilder app, bool isDevelopnment)
        {
            app.ConfigureSwagger();
            //app.ConfigurePersistence();
            return app;
        }
        //public static IApplicationBuilder Configure(this IApplicationBuilder app,
        //                                            IWebHostEnvironment webHostEnvironment,
        //                                            IConfiguration configuration)
        //{
        //    app.UsePathBase(configuration.GetSection("BasePath").Value);
        //    //app.ConfigureMVC(webHostEnvironment);
        //    //app.ConfigurePersistence();
        //    return app;
        //}
    }
}
