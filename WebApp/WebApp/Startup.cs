﻿using Bootstrap;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.IO;
using System.Runtime;

namespace WebApp
{
	internal class Startup
	{
		private readonly IConfiguration _configuration;

		public Startup(IConfiguration configuration)
		{
			_configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.ConfigureServices(_configuration);

			//services.AddTransient<IPresenter, Presenter>();

			//IdentityModelEventSource.ShowPII = true; //Add this line

			var version = _configuration.GetValue<string>("Configuration:App:Version");
			Console.WriteLine($"Version {version}");
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{
			//GCSettings.LatencyMode = GCLatencyMode.Batch;
			//GCSettings.LargeObjectHeapCompactionMode = GCLargeObjectHeapCompactionMode.CompactOnce;

			new ConfigurationBuilder()
				.SetBasePath(Directory.GetCurrentDirectory())
			//	.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
			//	//.AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true, reloadOnChange: true)
				.Build();
			app.Configure(env.IsDevelopment());
		}
	}
}
